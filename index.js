console.log("Test connection.");

// WHILE LOOP

let count  = 5;

while(count !==0){
	console.log("While: " + count);
	count--;
}

// DO WHILE LOOP
let number = Number(prompt("Give me a number"));

do{
	console.log("Do While: " + number);

	number += 1;

} while(number < 10);

// FOR LOOP

let myString = "Alex";

console.log(myString.length);


for (let x=0; x < myString.length; x++){
	console.log(myString[x]);
}

// Sample 2

let myName ="CasSanDra";

for ( let i = 0; i < myName.length; i++){
	console.log(myName[i].toLowerCase());

	if (
		myName[i].toLowerCase() == "a" ||
		myName[i].toLowerCase() == "e" ||
		myName[i].toLowerCase() == "i" ||
		myName[i].toLowerCase() == "o" ||
		myName[i].toLowerCase() == "u"
		) {
		console.log(3)
	} else {
		console.log(myName[i]);
	}
}

// CONTINUE and BREAK Statements
// Sample 1
for (let count = 0; count <= 20; count++) {

	if (count % 2 === 0){
		continue;
	}

	console.log("continue and break: " + count);

	if(count > 10){
		break;
	}
}

// Sample 2

let name="alexandro"
for (let i = 0; i < name.length; i++){
	console.log(name[i]);

	if(name[i].toLowerCase() === "a"){
		console.log("continue to next iteration");
		continue;
	}

	if(name[i] == "d"){
		break;
	}
}